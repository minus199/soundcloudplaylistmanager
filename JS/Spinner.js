/**
 * Created by minus on 2/16/15.
 */

    var SpinnerManager = function(){ this.currentSpinner = this.create() };

    SpinnerManager.prototype.create = function(){
        var spinnerElement = $("<div/>",{class: "spinners", id: "spinnerG"}).hide();
        for(var i = 1; i < 4; i++){
            spinnerElement.append($("<div/>", {id: "blockG_" + i, class: "spinner_blockG"}));
        }

        $("body").append(spinnerElement);

        return spinnerElement;
    };

    SpinnerManager.prototype.constructor = function() {this.create()};

    SpinnerManager.prototype.setToIndex = function (index){
        this.currentSpinner = $(".spinners").eq(index);
    };

    SpinnerManager.prototype.setToLast = function (){
        this.currentSpinner = $(".spinners").last();
    };

    SpinnerManager.prototype.destroy = function(){
        this.currentSpinner.remove();
    };

    SpinnerManager.prototype.show = function(){
        this.currentSpinner.show();
    };

    SpinnerManager.prototype.hide = function(){
        this.currentSpinner.hide();
    };

    SpinnerManager.prototype.toggle = function(){
        this.currentSpinner.toggle();
    };



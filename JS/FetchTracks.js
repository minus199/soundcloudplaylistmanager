    /**
     * Created by minus on 2/18/15.
     */
    var FetchManager = function () {
        var callback;

        this.setCallback = function (user_callback) {
            callback = user_callback
        };

        this.getCallback = function () {
            return callback;
        };
    };

    var likesOffset = 0;
    var page_size = 200;
    FetchManager.prototype._get = function (type) {
        var route = App.TracksManager.router(type);
        if (!route) {
            console.error("Unable to complete request for " + type + ". unable to find route.");
            return false;
        }

        SC.get(
            "/me/favorites.json",
            {limit: page_size, offset: likesOffset},
            function (tracks, error) {
                likesOffset = likesOffset + tracks.length;

                if (tracks.length === 0) {
                    return $.isFunction(App.TracksManager.getCallback()) ? App.TracksManager.getCallback()(App.DataObject.getTracks("likes")) : true;
                }

                App.DataObject.setTracks(type, tracks);
                App.TracksManager.likes();
            }
        );
    };

    FetchManager.prototype.likes = function () {
        return this._get("likes");
    };

    FetchManager.prototype.getSongsByTag = function (tag) {
        SC.get('/tracks', {tags: "break beat", offset: 50}, function (tracks) {
            App.TracksManager.currentTracks = tracks;
            App.PlayListBuilder.buildTableFor(tracks);
        });
    };

    FetchManager.prototype.getSongsByKeyword = function (keyword) {
        SC.get('/tracks', {q: keyword}, function (tracks) {
            App.TracksManager.currentTracks = tracks;
            $.each(tracks, function (index, track) {
                var data = [{id: track.id, text: track.title}];
                $("#songsContainer").select2({data: data});
            });
        });
    };
/**
 * Created by minus on 2/7/15.
 */

var PlayListBuilder = function () {
    this._prepareData = function (tracks) {
        var sounds = [];
        $.each(tracks, function (index, track) {
            sounds.push({
                value: track.id,
                label: track.title
            });
        });

        return sounds;
    };

    this.sortableData = {
        start: function (event, ui) {
        },
        stop: function (event, ui) {
        },
        revert: true,
        grid: [10, 10]
    };

    this.autoCompleteData = function (tracks) {
        return {
            /*autoFocus: true,*/
            source: this._prepareData(tracks),
            select: function (e, u) {
                console.log(e, u);
            },
            minLength: 0,
            create: function (event, ui) {
                $(this).autocomplete("widget").sortable(App.PlayListBuilder.sortableData);

                var likes = $("#likes");
                likes.data('ui-autocomplete')._renderItem = function (ul, item) {
                    var currentLI = $("<li>")
                        .attr("data-value", item.value)
                        .append($("<span/>", {"class": "shortText", text: item.label}))
                        .appendTo(ul);

                    App.Tools.titleTrimmer(currentLI);

                    return currentLI;
                };

                likes.data('ui-autocomplete')._resizeMenu = function () {
                    this.menu.element.outerWidth(700);
                };

                likes.data('ui-autocomplete')._renderMenu = function (ul, items) {
                    var that = this;
                    $.each(items, function (index, item) {
                        var $li = that._renderItemData(ul, item);

                        function isEven(item) {
                            return item.hasClass("evenPair");
                        }

                        function isOdd(item) {
                            return !isEven(item);
                        }

                        if (isOdd($li.prev().prev())) {
                            $li.addClass("evenPair");
                        }
                    });

                    var popOverManager = new PopoverManager();

                    that.menu.element.hoverIntent({
                        over: function () {
                            popOverManager.handleHover($(this));
                        },
                        out: function () {
                            popOverManager.HandleHoverOut($(this));
                        },
                        selector: '.ui-menu-item'
                    });

                    App.spinner.hide();
                };
            },
            focus: function (event, ui) {
                $('.ui-menu-item').draggable({
                    helper: "clone",
                    handle: ".full-text",
                    cancel: ".ui-menu-item",
                    dragstart: function (event, ui) {
                        console.log("starting drag");
                    }
                });
            }
        }
    };
};

// container = $("#likes") -- useful when i have $("#stream") for example
PlayListBuilder.prototype.generateSoundsList = function (container, tracks) {
    var that = this;

    container.autocomplete(that.autoCompleteData(tracks));

    $("#searchListContainer").toggle("slide", function () {
        $(".slider").toggle("slide");
        $(".ui-autocomplete").toggle("slide", function () {
            var likes = $("#likes");
            likes.focus();
            likes.autocomplete("search", "");
        });
    });

    this.appendEventListeners();
};

PlayListBuilder.prototype.appendEventListeners = function () {
    $('#selectedSounds').tagit();

    $("#searchListContainer").droppable({
        drop: function (event, ui) {
            $("#selectedSounds").tagit("createTag", $(ui.draggable[0]).attr("data-value"));
        },
        over: function (event, ui) {

        }
    });

    $("#volume").on("change mousemove", function () {
        soundManager.setVolume(App.TracksManager.currentlyPlaying.id, $(this).val());
    });
};
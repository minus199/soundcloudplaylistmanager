/**
 * Created by minus on 2/27/15.
 */
var Tools = function () {};

Tools.prototype.titleTrimmer = function (container) {
    var fullText = container.text();
    container.attr('data-sound-title', fullText);

    container = container.children("span");
    if (fullText.length > 30) {
        container.text(fullText.substring(0, 30) + "...");
    } else {
        var missingLength = 30 - fullText.length;

        var correctText = fullText;
        for (var i = 0; i < missingLength; i++) {
            correctText += " ";
        }

        container.text(correctText);
    }
};

Tools.prototype.objectLength = function (obj) {
    var count = 0;

    $.each(obj, function (key, value) {
        count += 1;
    });

    return count;
};

/* for counting objects that consists of multiple objects */   /* likes paginated tracks */
Tools.prototype.subObjLength = function (obj) {
    var count = 0;
    $.each(obj, function (key, value) {
        count += objectLength(value);
    });

    return count;
};

Tools.prototype.msToTime = function (s) {
    var ms = s % 1000;
    s = (s - ms) / 1000;
    var secs = s % 60;
    s = (s - secs) / 60;
    var mins = s % 60;
    var hrs = (s - mins) / 60;

    return (hrs < 10 ? "0" + hrs : hrs) + ":" + (mins < 10 ? "0" + mins : mins) + ':' + (secs < 10 ? "0" + secs : secs);
};
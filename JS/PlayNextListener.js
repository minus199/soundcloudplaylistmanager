/**
 * Created by minus on 2/16/15.
 */




/*_Player.prototype._selectList = function(){
    function _clean($var) {
        if (/\S/.test($var)) {
            return true;
        }
    }

    function _split($var){
        $var = $var.split(":", 2);
        return {id: parseInt($var.shift()), title: $var.pop()};
    }

    return $("#playlist").text().split(" | ").filter(_clean).map(_split);

};*/

var _Player = function() {
    this.currentPos = 0;
};

_Player.prototype.play = function(){
    $("#currentlyPlaying")
        .text(App.selection[Player.currentPos])
        .attr('trackID', Player.selectedTracks[Player.currentPos].id);


    $("#playingNext")
        .text(Player.selectedTracks[Player.currentPos+1].title)
        .attr('trackID', Player.selectedTracks[Player.currentPos+1].id);
};

var Player = new _Player();


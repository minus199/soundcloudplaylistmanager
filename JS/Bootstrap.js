/**
 * Created by minus on 2/7/15.
 */
var Bootstrap = function () {
    window.onerror = function (e) {
        App.spinner.hide();
        console.error("Unable to connect or some failure. refresh the page bitch.", e);
        return false;
    };

    this.spinner = new SpinnerManager();
    this.selection = {};

    this.appendStyleTags();
};

Bootstrap.prototype._populate = function () {
    App.Tools = new Tools();
    App.DataObject = new DataObject();
    App.PlayListBuilder = new PlayListBuilder();
    App.PlayControls = new PlayControls();
    App.TracksManager = new TracksManager();
};

Bootstrap.prototype.appendStyleTags = function () {
    var css = [
        "Vendor/bootstrap-3.3.2-dist/css/bootstrap",
        "jquery-ui-with-theme",
        "CSS/spinner2",
        /*"Vendor/theme2/jquery-ui",*/
        "Vendor/tag-it/css/jquery.tagit",
        "Vendor/bootstrap-slider/css/slider",
        "CSS/body",
        "CSS/main"
    ];

    $.each(css, function (index, file) {
        $("head").append($("<link/>", {rel: "stylesheet", type: "text/csv", href: file + ".css"}))
    });
};

Bootstrap.prototype._connect = function () {
    App.spinner.show();

    SC.initialize({
        client_id: App.DataObject.creds.client_id,
        client_secret: App.DataObject.creds.client_secret,
        redirect_uri: 'http://minus.vagrant.oht.cc/SoundcloudPlaylistManager/callback.html'
    });

    SC.connect(function (user, error) {
        if (error) {
            console.error(error);
            return;
        }

        window.user = user;
        //console.info(user.name + " is Connected.");
        App._doAfter();
    });
};

Bootstrap.prototype._doAfter = function () {
    App.TracksManager.setCallback(function (tracks) {
        App.PlayListBuilder.generateSoundsList($("#likes"), tracks);
        //App.PlayListBuilder.buildTableFor(tracks);
    });

    App.TracksManager.likes();
};

var App;

$(document).ready(
    function () {
        App = new Bootstrap();
        App._populate();
        App._connect();
        $("#volume").slider({
            min: 0,
            max: 100,
            step: 1,
            value: 30,
            stop: function (event, ui) {

                console.log(ui.value);
            }
        });

        $("#volume").on("slideStop", function (ev) {
            if (App.TracksManager.currentlyPlaying) {
                soundManager.setVolume(App.TracksManager.currentlyPlaying.id, ev.value);
            }
        });

        var css = {
            position: "absolute",
            top: "100px",
            "z-index": 10000000000,
            left: "370px"
        };

        var p = App.PlayControls.controlsConfig.seekControls;
        $("#searchListContainer").append(App.PlayControls.createPlayControls(p).css(css).removeClass("playback-ctrls"));
    }
);
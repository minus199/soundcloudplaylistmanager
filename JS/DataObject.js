/**
 * Created by minus on 2/7/15.
 */
var DataObject = function () {
    this.creds = {
        client_id: "2c1a94f6da22acde93cac684299ecbd0",
        client_secret: "c9fbeb09e6767721defdf3ef64e19b81"
    };


    this.likesByID = {};
    this.likesByIdPaginated = [];

    this.metadataElements = [
        {val: 'genre', klass: "meta-data"},
        {val: 'created_at', klass: "meta-data"},
    ];

    var tracksContainer = {
        current: [],
        likes: [],
        byTag: []
    };

    this.setTracks = function (type, newTracks){
        var sorted = {};
        $.each(newTracks, function (index, track) {
            sorted[track.id] = track;
        });

        $.extend(App.DataObject.likesByID, sorted);
        App.DataObject.likesByIdPaginated.push(sorted);

        tracksContainer[type] = tracksContainer[type] ? tracksContainer.current = tracksContainer[type].concat(newTracks) : newTracks;
    };

    this.getTracks = function (type){
        return tracksContainer[type] != undefined && tracksContainer[type].length > 0 ? tracksContainer[type] : false;
    };
};
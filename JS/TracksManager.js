/**
 * Created by minus on 2/14/15.
 */
    var TracksManager = function() {
        this.routes = {
            likes: "/me/favorites.json"
            //"/tracks"
        };

        $.extend(this, new FetchManager);

        this.currentlyPlaying = null;
    };

    TracksManager.prototype.router = function(type){
        return this.routes[type] != undefined && this.routes[type].length > 0 ? this.routes[type] : false;
    };

    TracksManager.prototype.sortIntoCols = function(){
        var groupSize = App.TracksManager.currentTracks.length / 3;
        var lastItemInGroupSize =  groupSize;

        if (groupSize%3 == 0){
            ++lastItemInGroupSize;
            Math.floor(groupSize);
        }

        return [
            App.TracksManager.currentTracks.slice(0, groupSize),
            App.TracksManager.currentTracks.slice(groupSize, groupSize * 2),
            App.TracksManager.currentTracks.slice(groupSize * 2, groupSize * 2 + lastItemInGroupSize)
        ];
    };


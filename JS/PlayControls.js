    /**
     * Created by minus on 2/7/15.
     */
    var PlayControls = function () {
        /* If element inside ControlTypes is array, the meaning is that this sub array will be toggled -- i.e. play:pause */
        this.globalControlTypes = ['seek-first', 'seek-prev', ['play', 'pause'], 'stop', 'seek-next', 'seek-end', ['volume-off', 'volume-on'], 'close'];
        this.perTrackControlTypes = [['play', 'pause']];
        this.listPointer = 0;

        this.controlsConfig = {
            iconClass: "playback-ctrls bs-glyphicons",
            /*iconClass: "ui-state-default ui-corner-all",*/
            baseClass: "glyphicon glyphicon-",
            /* baseClass: "ui-icon ui-icon" */
            controls: [
                ["play", "pause"],
                "stop"
            ],
            seekControls: {
                "fast-backward": "firstSound",
                "step-backward": "previousSound",
                "backward": "skipBackward",
                "forward": "skipForward",
                "step-forward": "nextSound",
                "fast-forward": "lastSound"
            },
            volume: [
                "volume-off",
                "volume-down",
                "volume-up",
            ],
            tempFolder: ["chevron-left", "chevron-right", "thumbs-up", "thumbs-down", "save", "cd"]
        };

        var onToggleableClick = function (iconContainerElement) {
            var currentClass = App.PlayControls.controlsConfig.baseClass;

            var defaultState = iconContainerElement.attr('defaultState');
            var defaultStateClass = currentClass + defaultState;

            var toggledState = iconContainerElement.attr('toggledState');
            var toggledStateClass = currentClass + toggledState;

            var currentState = "";
            var iconElement = iconContainerElement.children().first();
            if (iconElement.hasClass(defaultStateClass)) {
                iconElement.attr("class", toggledStateClass);
                currentState = defaultState;
            } else {
                iconElement.attr("class", defaultStateClass);
                currentState = toggledState;
            }

            /* Works from any element inside a .ui-menu-item element */
            var soundCloudID = iconContainerElement.parents(".ui-menu-item").attr("data-value");
            App.PlayControls[currentState](soundCloudID);
        };

        $("body").on("click", ".subMenu.play", function () {
            var currentClass = App.PlayControls.controlsConfig.baseClass;
            var defaultState = $(this).attr('defaultState');
            var defaultStateClass = currentClass + defaultState;

            $(this).siblings().toggleClass("class", defaultStateClass);

            var a = $(this).siblings().removeClass("play");
            a.addClass("pause");

            onToggleableClick($(this));
        });

        var that = this;
        $.each(that.controlsConfig.seekControls, function (actionName, action) {
            $("body").on("click", ".subMenu." + action, function () {
                var soundCloudID = $(this).parents(".ui-menu-item").attr("data-value");
                App.PlayControls[action](soundCloudID);
            });
        });
    };

    PlayControls.prototype.createPlayControls = function (controlTypes, trackID) {
        var createToggleableElement = function (defaultState) {
            /* how do i pass this fucking var into click(); */
            var button = $("<span/>");
            button.addClass(App.PlayControls.controlsConfig.baseClass + defaultState);

            return button;
        };

        var createNonToggleableElement = function (type) {
            var button = $("<span/>");
            button.addClass(App.PlayControls.controlsConfig.baseClass + type);

            return button;
        };

        var playControlsBar = $("<ul/>");
        playControlsBar.addClass(App.PlayControls.controlsConfig.iconClass);

        $.each(controlTypes, function (index, type) {
            var buttonContainer = $("<li/>");
            buttonContainer.attr("soundID", trackID);

            var button;
            switch (Object.prototype.toString.call(type)) {
                case '[object Array]':
                    /* Toggleable element */
                    buttonContainer
                        .addClass("subMenu " + type[0])
                        .attr("defaultState", type[0])
                        .attr("toggledState", type[1]);
                    button = createToggleableElement(type[0]);
                    break;
                case '[object String]':
                    /* Non-Toggleable element */
                    button = createNonToggleableElement(index);
                    buttonContainer.addClass("subMenu " + type);
                    break;
            }

            playControlsBar.append(buttonContainer.append(button));
        });

        return playControlsBar;
    };

    PlayControls.prototype.createSound = function (trackID, autoPlay, autoLoad) {
        autoPlay = autoPlay ? autoPlay : false;
        var $clonedProgress = $("#cloneableProgress").clone();
        $clonedProgress
            .appendTo($("#cloneableProgress").parent())
            .show("slide")
            .attr("for", trackID)
            .removeAttr("id");

        $clonedProgress.find(".progress-bar").text(App.DataObject.likesByID[trackID].title);

        soundManager.createSound(
            {
                id: trackID,
                url: "https://api.soundcloud.com/tracks/" + trackID + "/stream?oauth_token=" + SC.accessToken(),
                autoLoad: autoLoad ? autoLoad : true,
                autoPlay: false,
                volume: $("#volume").slider('getValue').val(),
                whileloading: function () {
                    var currentProgress = this.bytesLoaded * 100 / this.bytesTotal;
                    if ($clonedProgress && currentProgress > 98) {
                        $clonedProgress.hide("slide", function () {
                            $(this).remove();
                        });
                        $clonedProgress = null;
                        return;
                    }

                    if ($clonedProgress)
                        $clonedProgress.find(".progress-bar").css("width", currentProgress + "%");
                },
                onsuspend: function () {
                    //$clonedProgress.removeClass("active");
                },
                onbufferchange: function () {

                },
                onfinish: function () {

                },
                onload: function () {
                    $clonedProgress.addClass("active");
                    if (!autoPlay)
                        return;

                    var currentSound = soundManager.getSoundById(trackID);
                    App.PlayControls.play(currentSound);
                }
            }
        );
    };

    PlayControls.prototype.play = function (sound, _callback) {
        this.playCallback = _callback;
        soundManager.pauseAll();

        var $liElement = $(".ui-menu-item[data-value='" + sound.id + "']");

        var playOptions = {
            onplay: function () {
                if (App.TracksManager.currentlyPlaying = sound){
                    $liElement.animate({"left": "100%", "z-index": "1000000"},function(){$(this).mouseenter()});
                }
            },
            whileplaying: function () {
                var currentProgressRatio = parseFloat(this.position / this.duration).toFixed(3) * 100;
                var progressBar = $liElement.children().find(".progress-bar");
                progressBar
                    .attr("aria-valuenow", this.position)
                    .css("width", currentProgressRatio + "%");

                progressBar.children(".timer").text(App.Tools.msToTime(this.position));
                //console.log('sound '+this.id+' playing, '+this.position+' of '+this.duration);
            },
            onfinish: function () {
                App.TracksManager.currentlyPlaying = null;
                console.log("cleared " + this.id + " from currently playing");
            }
        };

        if (sound !== null && typeof sound === 'object') {
            if (sound.paused){
                sound.resume();
            } else {
                sound.play(playOptions);
            }

            return true;
        }

        if (!isNaN(sound)){
            var soundObj = soundManager.getSoundById(sound);
            if (soundObj){
                soundObj.resume();
            } else {
                this.createSound(sound, true);
            }
        }
    };

    /**
     *
     * @param sound SMSound|int
     * @param _callback
     */
    PlayControls.prototype.pause = function (sound, _callback) {
        /*_callback = _callback ? _callback : function () {};
         track.pause({onfinish: _callback});*/
        if (sound !== null && typeof sound === 'object') {
            sound.pause();
            return true;
        }

        if (!isNaN(sound)) {
            soundManager.getSoundById(sound).pause();
        }
    };

    PlayControls.prototype.stop = function (sound) {
        App.TracksManager.currentlyPlaying = null;

        if (sound !== null && typeof sound === 'object') {
            sound.stop();
            return true;
        }

        if (!isNaN(sound)) {
            soundManager.getSoundById(sound).stop();
        }
    };

    PlayControls.prototype.nextSound = function () {
        var $currentLi = $("[data-value='" + App.TracksManager.currentlyPlaying.id + "']");
        var $nextLi = $currentLi.next();
        var toBePlayedSID = $nextLi.next().attr("data-value");

        console.log("next" , toBePlayedSID);

        this.play(toBePlayedSID);
    };

    PlayControls.prototype.previousSound = function (soundID) {
        var $currentLi = $("[data-value='" + App.TracksManager.currentlyPlaying.id + "']");
        var $prevLi = $currentLi.prev();
        var toBePlayedSID = $prevLi.prev().attr("data-value");

        console.info("prevSound was pressed", toBePlayedSID);
        this.play(toBePlayedSID);
    };

    PlayControls.prototype.skipBackward = function () {
        console.info("skipBackward was pressed");
    };

    PlayControls.prototype.skipForward = function () {
        console.info("skipForward  was pressed");
    };

    PlayControls.prototype.firstSound = function () {
        console.info("firstSound was pressed");
    };
    PlayControls.prototype.lastSound = function () {
        console.info("lastSound  was pressed");
    };

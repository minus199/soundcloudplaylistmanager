    /**
     * Created by minus on 3/1/15.
     */

    var PopoverManager = function () {
    };

    PopoverManager.prototype.handleHover = function (parentElement) {
        var currentSoundID = parentElement.attr("data-value");
        this.currentSound = App.DataObject.likesByID[currentSoundID];

        this.artworkURL = this.currentSound.artwork_url ? this.currentSound.artwork_url : "IMG/no_artwork.png";

        /* Element already have a popover */
        if (parentElement.children(".scrollWrapper").length) {
            this.showClonedPopover(parentElement);
            return;
        }

        this.clonePopover(parentElement);
    };

    PopoverManager.prototype.HandleHoverOut = function (parentElement) {
        var currentSoundID = parentElement.attr("data-value");
        if (App.TracksManager.currentlyPlaying && soundManager.getSoundById(currentSoundID) == App.TracksManager.currentlyPlaying){
            if (!App.TracksManager.currentlyPlaying.paused){
                parentElement.animate({"left": "100%"});
                return;
            }
        }

        parentElement.animate({"left":  "initial"});
        $(".scrollWrapper").hide("slide");
    };

    PopoverManager.prototype.clonePopover = function (parentElement) {
        var that = this;

        var scrollWrapper = $("#cloneableScrollWrapper").clone();
        scrollWrapper.removeAttr("id");

        var $scrollMarquee = scrollWrapper.children().first();
        scrollWrapper.children().find(".sound-title").eq(0)
            .text(this.currentSound.title)
            .css({
                position: "absolute",
                top: 0,
                display: "initial"
                /*left: "110px"*/
            });

        var $durationContainer = $("<span/>", {"class": "duration"});
        $durationContainer.text(App.Tools.msToTime(this.currentSound.duration));
        var $positionContainer = $("<span/>", {"class": "timer"});

        var $progressBar = $("<div/>", {
            "class": "progress-bar",
            role: "progressBar",
            "aria-valuenow": 0,
            "aria-valuemin": "0",
            "aria-valuemax": this.currentSound.duration
        });

        $progressBar.append($positionContainer).append($durationContainer);

        $scrollMarquee
            .append(that.createPlayControls(that.currentSound.stream_url))
            .append($("<div/>", {"class": "soundProgress progress"}).append($progressBar));

        this.createArtworkContainer($scrollMarquee);

        scrollWrapper.appendTo(parentElement).hide();
        scrollWrapper.show().css("z-index", "1000000");
    };

    PopoverManager.prototype.showClonedPopover = function (parentElement) {
        //submenu item by track id -- $("[data-value='193489533']")
        var scrollWrapper = parentElement.children(".scrollWrapper").eq(0);
        scrollWrapper.css("z-index", "1000000").show();
    };

    PopoverManager.prototype.createArtworkContainer = function (parentElement) {
        var $img = $("<img />");
        var imgStyle = {
            "display": "inline-block",
            "visibility": "visible",
            height: "100px",
            width: "100px",
            "border-radius": "10px"
        };

        $img.css(imgStyle);
        parentElement.prepend($img);

        var artworkURL = this.artworkURL;
        $.ajax({
            url: artworkURL,
            cache: true,
            processData: false
        }).always(function (b64data) {
            $img.attr("src", artworkURL).fadeIn();
        });
    };

    PopoverManager.prototype.createPlayControls = function () {
        var playControls = App.PlayControls.createPlayControls(App.PlayControls.controlsConfig.controls);
        playControls.addClass("mini-ctrls");
        return playControls;
    };

    PopoverManager.prototype.create = function (parentElement) {
        var scrollWrapper;
        /* Create new */
        scrollWrapper = $("<div/>", {id: "fullTextContainer", "class": "scrollWrapper"});
        scrollWrapper.css({
            "background-image": "url(" + this.currentSound.waveform_url + ")",
            background: "-webkit-linear-gradient(top, #b0d4e3 0%, #88bacf 100%)",
            "z-index": 100000
        });
        scrollWrapper.appendTo(parentElement).hide();

        var marqueeWrapper = $("<span />", {"class": "scrollMarquee"});
        /* Append play controls */
        var playControls = this.createPlayControls(this.currentSound.stream_url).appendTo(marqueeWrapper);

        /* Append image -- will be appended before play controls  */
        this.createArtworkContainer(marqueeWrapper);

        /* Create title element inside popover */
        var $shortText = $("<span/>", {"class": "sound-title", text: this.currentSound.title});
        $shortText.css("display", "inline-flex")
        $shortText.insertBefore(playControls);

        marqueeWrapper.appendTo(scrollWrapper);

        scrollWrapper.css({
            "-webkit-box-shadow": "3px 9px 13px -9px rgba(0,0,0,1)",
            "border-radius": "10px"
        }).show("blind", 100);
    };